import React from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import NotFoundPage from "../components/NotFoundPage";
import Header from "../components/Header";
import HomePage from "../components/HomePage";
import ContactPage from "../components/ContactPage";
import Portfolio from "../components/Portfolio";
import Item from "../components/Item";

const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Header />
            <Switch>
                <Route path="/" exact={true} component={HomePage}/> {/*need to set exact otherwise will show welcome page for any URL that extends the root URL*/}
                <Route path="/portfolio" exact={true} component={Portfolio}/>
                <Route path="/portfolio/:id" component={Item}/> {/*need to specify id prop here for props to be passed to ItemOne component to be dynamically interpolated in to JSX fragment*/}
                <Route path="/contact" component={ContactPage}/>
                <Route component={NotFoundPage}/>
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;
