import React from "react";
import PortfolioHeader from "./PortfolioHeader";

const Portfolio = () => {
    return (
        <div>
            <h1>My Work</h1>
            <p>Checkout the following things I've done</p>
            <PortfolioHeader/>
            {/*<Link  to="/portfolio/1">Item One</Link> alternative structure - see PortfolioHeader.js*/}
            {/*<Link  to="/portfolio/2">Item Two</Link>*/}
        </div>
    )
};

export default Portfolio;
