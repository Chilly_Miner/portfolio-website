import React from "react";

const ContactPage = () => (
    <div>
        <h1>Contact</h1>
        <p>Contact me at test@test.com</p>
    </div>
);

export default ContactPage;
