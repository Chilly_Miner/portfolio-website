import React from "react";

const Item = (props) => {
    console.log(props); //checks props have been passed in by explicitly returning JSX then logging props
    return (
        <div>
            <h1>Here's something I've done</h1>
            <p>This page is for the item with id of {props.match.params.id}</p>
        </div>
    )
};


export default Item;
