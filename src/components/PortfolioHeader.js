import React from "react";
import {Link} from "react-router-dom";
//to be honest, did not need a header as it is not being displayed constantly like our normal Header - could have instead just moved Link tags to Portfolio, but this still works
const PortfolioHeader = () => (
    <header>
        <Link  to="/portfolio/1">Item One</Link> {/*This link casues URL to change which then calls Item prop as path="/portfolio/:id" is true (AppRouter.js)*/}
        <Link  to="/portfolio/2">Item Two</Link> {/*No need to use NavLink as not styling here*/}
    </header>
);
export default PortfolioHeader;
